#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "drv_i2c.h"
#include "util_ioctl.h"
#include "at24c02.h"
#include "drv_i2c_typedefs.h"

#define SUPPORT_IT_DEBUG
#ifdef SUPPORT_IT_DEBUG
#define IT_DEBF(fmt, arg...) printf(fmt, ##arg)
#else
#define IT_DEBF(fmt, arg...)
#endif

#define FRC_I2C_MASTER_INDEX        0




/***6251 register table select***/
//#define CONTROl_BLONOFF_BYI2C
//#define N173HGE_E11_1920X1080


static I2C_INFO _IT_I2C_INFO;
static INT32 _IT_I2C_DEVICE = -1;

typedef struct _FRC_I2C_AT24C02
{
	UINT8 iteSlaveAddr;
	UINT8 iteRegAddr;
	UINT8 iteData;
}FRC_I2C_AT24C02;


FRC_I2C_AT24C02 AT24C02_Table[] =
{	
	{0x52,0xff,0x81},//register bank
	{0x52,0x00,0x04}, 

  	
	{0x52,0x18,0x01},
	{0x52,0xff,0x80},//register bank
	{0x52,0x94,0x08},
};

void delay(UINT32 temp)
{

  while(temp--)
  	{
        ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
		
		IT_DEBF("@%s: I2C write delay !!\r\n", __FUNCTION__);
    }


}






 INT8 _IT_AT24C02_I2COpen(void)
{
    if (-1 == _IT_I2C_DEVICE)
    {
        _IT_I2C_DEVICE = open("/dev/sisi2c", O_RDWR);
        _IT_I2C_INFO.MasterIndex = FRC_I2C_MASTER_INDEX;
      //  _IT_I2C_INFO.SlaveAddress = IT6251_ADDR1;
        _IT_I2C_INFO.BaseAddrType = ADDRESS_TYPE_WORD; 
        _IT_I2C_INFO.Speed = I2C_SPEED_100K;
    }
    IT_DEBF("\n%s, _IT_I2C_DEVICE = %d\n", __FUNCTION__, _IT_I2C_DEVICE);

    return 1;
}

//--------------------------------------------------------------------------------------------------
//
// Hardware Interface
//

DRV_Status_t AT24C02_Reg_Read(UINT32 SlaveAddr,UINT32 RegAddr, UINT8 *pData, UINT16 Length)
{	
    INT32 i32Status = 0;
    UINT8 i = 1;
    UINT32 i2c_base_addr;
    
    i2c_base_addr = 0x02000000|(RegAddr&0xffff);

    _IT_I2C_INFO.SlaveAddress= (UINT32)SlaveAddr;
    _IT_I2C_INFO.BaseAddress = (UINT32)i2c_base_addr;
    _IT_I2C_INFO.Data = pData;
    _IT_I2C_INFO.DataLength = Length;
    _IT_I2C_INFO.BaseAddrType = ADDRESS_TYPE_MULTIPLE; 

    do {
        i32Status = ioctl(_IT_I2C_DEVICE, IOC_I2C_MASTER_READ, &_IT_I2C_INFO);
        if (i32Status < 0)
        {
            printf("@%s: I2C read NG!!\n", __FUNCTION__);
			return DRV_ERR_FAILURE;
        }
    } while (--i);

    return DRV_SUCCESS;
}

DRV_Status_t AT24C02_Reg_Write(UINT32 SlaveAddr,UINT32 RegAddr, UINT8 *pData, UINT16 Length)
{
    INT32 i32Status;
    UINT8   *tmp=malloc(sizeof(INT8)*(Length)+2);//+2=> 2 byte Addr
    UINT8   t;
	
    if(Length <1)
    {
        IT_DEBF("@%s:Length error !!\n", __FUNCTION__);
        free(tmp);
        return DRV_ERR_FAILURE;
    }
    memcpy(&tmp[0], &RegAddr, 2);  
    memcpy(&tmp[2], pData, Length);
    t=tmp[0];
	tmp[0]=tmp[1];
	tmp[1]=t;


	
    _IT_I2C_INFO.SlaveAddress= (UINT32)SlaveAddr;
    //_IT_I2C_INFO.BaseAddress = (UINT32)RegAddr;
    _IT_I2C_INFO.Data = tmp;
    _IT_I2C_INFO.DataLength = Length+2;
    _IT_I2C_INFO.BaseAddrType = ADDRESS_TYPE_NONE; 
    
    i32Status = ioctl(_IT_I2C_DEVICE, IOC_I2C_MASTER_WRITE, &_IT_I2C_INFO);
    if (i32Status < 0)
    {
       IT_DEBF("@%s: I2C write NG!!\n", __FUNCTION__);
       free(tmp);
        return DRV_ERR_FAILURE;
    }
    free(tmp);
    return DRV_SUCCESS;
}

void AT24C02_init(void)
{
    UINT16 i;
    UINT8 uc;
    for (i = 0; i < sizeof(AT24C02_Table)/sizeof(FRC_I2C_AT24C02); i++)
    {						
        uc = AT24C02_Reg_Write(AT24C02_Table[i].iteSlaveAddr,AT24C02_Table[i].iteRegAddr, &AT24C02_Table[i].iteData, 1);
        if (uc == 0)
        {
            IT_DEBF("AT24C02_init..........OK.......................%d.......[%x][%x][%x]................\n\n\n\n", i,AT24C02_Table[i].iteSlaveAddr, AT24C02_Table[i].iteRegAddr, AT24C02_Table[i].iteData);
        }
        else
        {
            IT_DEBF("AT24C02_init.........FAIL.......................%d......[%x][%x][%x]..................\n\n\n\n", i,AT24C02_Table[i].iteSlaveAddr, AT24C02_Table[i].iteRegAddr, AT24C02_Table[i].iteData);
        }

    }
    IT_DEBF("AT24C02_init................................................................................\n");

}



DRV_Status_t AT24C02_Reg_Write_Page(UINT32 SlaveAddr,UINT32 RegAddr, UINT8 *pData, UINT16 Length)
{

    UINT8 NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0;
    Addr = RegAddr % AT24C32_PAGE_SIGZ;//写入地址是每页的第几位
    count = AT24C32_PAGE_SIGZ - Addr;//在开始的一页要写入的个数
    NumOfPage = Length / AT24C32_PAGE_SIGZ;//要写入的页数
    NumOfSingle = Length % AT24C32_PAGE_SIGZ;//不足一页的个数


 
    /* If RegAddr is AT24C32_PAGE_SIGZ aligned */
    if(Addr == 0) //写入地址是页的开始
     {
        /* If Length < AT24C32_PAGE_SIGZ */
        if(NumOfPage == 0) //数据小于一页
         {
           //I2C_EE_PageWrite(pData, RegAddr, NumOfSingle);//写少于一页的数据
		   if(AT24C02_Reg_Write(SlaveAddr,RegAddr,pData,NumOfSingle)!=DRV_SUCCESS)
		   	{
		   	
			IT_DEBF(" 0 fail AT24C02_Write NumofPage=%d\r\n n",NumOfPage);
		   	return DRV_ERR_FAILURE;
		   	}
		   	else
			{
		     delay(0x20);
		     IT_DEBF(" 0 success AT24C02_Write NumofPage=%d\r\n",NumOfPage);
		   	}
         }
        /* If Length > AT24C32_PAGE_SIGZ */
       else //数据大于等于一页
        {
          while(NumOfPage--)//要写入的页数
          {

		    
			IT_DEBF("  AT24C02_Write init RegAddr=%d\r\n",RegAddr);
            //I2C_EE_PageWrite(pData, RegAddr, AT24C32_PAGE_SIGZ); //写一页的数据
			if(AT24C02_Reg_Write(SlaveAddr,RegAddr,pData,AT24C32_PAGE_SIGZ)!=DRV_SUCCESS)
				{
				
					IT_DEBF(" 1 faile AT24C02_Write NumofPage=%d\r\n",NumOfPage);
					return DRV_ERR_FAILURE;
				}
				else
				{
					delay(AT24C32_DELAY_TIME);
					IT_DEBF(" 1 success AT24C02_Write NumofPage=%d\r\n",NumOfPage);
				}

				
			IT_DEBF("  AT24C02_Write after RegAddr=%d\r\n",RegAddr);
            RegAddr += AT24C32_PAGE_SIGZ;
            pData += AT24C32_PAGE_SIGZ;
          }
 
         if(NumOfSingle!=0)//剩余数据小于一页
         {
           //I2C_EE_PageWrite(pData, RegAddr, NumOfSingle);//写少于一页的数据
		   if(AT24C02_Reg_Write(SlaveAddr,RegAddr,pData,NumOfSingle)!=DRV_SUCCESS)
		   	{
		   	
			IT_DEBF(" 2 fail AT24C02_Write NumofPage=%d\r\n",NumOfPage);
			   return DRV_ERR_FAILURE;
		   	}
           
		   IT_DEBF(" 2 success AT24C02_Write NumofPage=%d\r\n",NumOfPage);
		   //delay(0x20);
         }
       }
     }
/* If RegAddr is not AT24C32_PAGE_SIGZ aligned */
     else //写入地址不是页的开始
      {
          /* If Length < AT24C32_PAGE_SIGZ */
          if(NumOfPage== 0) //数据小于一页
           {
                    if(Length<=count)
                        {
                            //I2C_EE_PageWrite(pData, RegAddr, NumOfSingle);//写少于一页数据
							if(AT24C02_Reg_Write(SlaveAddr,RegAddr,pData,NumOfSingle)!=DRV_SUCCESS)
								{
								
								IT_DEBF(" 3 fail AT24C02_Write NumofPage=%d\r\n",NumOfPage);
							return DRV_ERR_FAILURE;
								}
							//delay(0x20);
							IT_DEBF(" 3 success AT24C02_Write NumofPage=%d\r\n",NumOfPage);

                        }
                        else
                        {
                              Length -= count;
                              //I2C_EE_PageWrite(pData, RegAddr, count);//将开始的空间写满一页
							  if(AT24C02_Reg_Write(SlaveAddr,RegAddr,pData,count)!=DRV_SUCCESS)
							  	{
							  	
								IT_DEBF(" 4 fail AT24C02_Write NumofPage=%d\r\n",NumOfPage);
							  return DRV_ERR_FAILURE;
							  	}
							  delay(AT24C32_DELAY_TIME);
							  IT_DEBF(" 4 success AT24C02_Write NumofPage=%d\r\n",NumOfPage);

                               RegAddr += count;
                               pData += count;
 
                               //I2C_EE_PageWrite(pData, RegAddr, Length);//在下一页写
							   if(AT24C02_Reg_Write(SlaveAddr,RegAddr,pData,Length)!=DRV_SUCCESS)
							   	{
							   	
								IT_DEBF(" 5 fail AT24C02_Write NumofPage=%d\r\n",NumOfPage);
							   return DRV_ERR_FAILURE;
							   	}
							   
							   IT_DEBF(" 5 success AT24C02_Write NumofPage=%d\r\n",NumOfPage);
							   //delay(0x20);

                        }
           }
          /* If Length > AT24C32_PAGE_SIGZ */
          else//数据大于等于一页
            {
              Length -= count;
              NumOfPage = Length / AT24C32_PAGE_SIGZ; //重新计算要写入的页数
              NumOfSingle = Length % AT24C32_PAGE_SIGZ;//重新计算不足一页的个数
     
              if(count != 0)//在此处count一定不为0，此判断条件好象有点多余
              {
                //I2C_EE_PageWrite(pData, RegAddr, count);//将开始的空间写满一页
                if(AT24C02_Reg_Write(SlaveAddr,RegAddr,pData,count)!=DRV_SUCCESS)
                	{
                	
					IT_DEBF(" 6 fail AT24C02_Write NumofPage=%d\r\n n",NumOfPage);
				return DRV_ERR_FAILURE;
                	}
				
				IT_DEBF(" 6 success AT24C02_Write NumofPage=%d\r\n n",NumOfPage);
				delay(AT24C32_DELAY_TIME);
                RegAddr += count;
                pData += count;
              }
     
              while(NumOfPage--)//要写入的页数
              {
                //I2C_EE_PageWrite(pData, RegAddr, AT24C32_PAGE_SIGZ);//写一页的数据
                if(AT24C02_Reg_Write(SlaveAddr,RegAddr,pData,AT24C32_PAGE_SIGZ)!=DRV_SUCCESS)
                	{
                	
					IT_DEBF(" 7 fail AT24C02_Write NumofPage=%d\r\n n",NumOfPage);
				return DRV_ERR_FAILURE;
                	}
				
				IT_DEBF(" 7 success AT24C02_Write NumofPage=%d\r\n n",NumOfPage);
				delay(AT24C32_DELAY_TIME);
                RegAddr += AT24C32_PAGE_SIGZ;
                pData += AT24C32_PAGE_SIGZ;
              }
             if(NumOfSingle != 0)//剩余数据小于一页
             {
               //I2C_EE_PageWrite(pData, RegAddr, NumOfSingle); //写少于一页的数据
               if(AT24C02_Reg_Write(SlaveAddr,RegAddr,pData,NumOfSingle)!=DRV_SUCCESS)
               	{
               	
				IT_DEBF(" 8 fail AT24C02_Write NumofPage=%d\r\n n",NumOfPage);
			   return DRV_ERR_FAILURE;
               	}
			   
			   IT_DEBF(" 8 success AT24C02_Write NumofPage=%d\r\n n",NumOfPage);
			   //delay(0x20);
             }
          }
     }
        /* Send STOP condition */

		return DRV_SUCCESS;
}



DRV_Status_t AT24C02_Reg_Read_Page(UINT32 SlaveAddr,UINT32 RegAddr, UINT8 *pData, UINT16 Length)
{


   if(AT24C02_Reg_Read(SlaveAddr,RegAddr,pData,Length)!=DRV_SUCCESS)
      return DRV_ERR_FAILURE;


     return DRV_SUCCESS;

}

DRV_Status_t AT24C02_Reg_Earse_Page(UINT32 SlaveAddr)
{    UINT32 i;
	 UINT32 RegAddr=0;
	 UINT16 page =AT24C32_INIT_LENGTH/AT24C32_PAGE_SIGZ;
	 UINT8 data[AT24C32_INIT_LENGTH]={0xFF};
	 UINT8 *pBuffer = NULL;
	 for(i=0;i<AT24C32_INIT_LENGTH;i++)
          {
          	data[i]=0xFF;
	 	}

			pBuffer=data;
 
            while(page--)
            	{
			        if(AT24C02_Reg_Write(SlaveAddr,RegAddr,pBuffer,AT24C32_PAGE_SIGZ)!=DRV_SUCCESS)
			        {
						  IT_DEBF("@%s: I2C write Earse faile Page page=%d !!\r\n", __FUNCTION__,page);
						  //return DRV_ERR_FAILURE;
			        }
					else
					{
						 IT_DEBF("@%s: I2C write Earse success Page page=%d !!\r\n", __FUNCTION__,page);
					}
					RegAddr += AT24C32_PAGE_SIGZ;
					pBuffer += AT24C32_PAGE_SIGZ;

				    delay(0x20);

            	}


     return DRV_SUCCESS;

}










