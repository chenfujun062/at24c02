
#ifndef __LVDS_CONVERTOR__
#define __LVDS_CONVERTOR__
#include <types.h>
#include "drv_i2c.h"
#include "drv_cvd2_external.h"
//#include "drv_i2c_main.h"
#include "util_ioctl.h"
//....
//void lvds_convertor_init(void);
//UINT8 DRV_Panel_I2cWrite(UINT8 ChipAddr, UINT8 SubAddr, UINT8 RegRVal);


#define AT24C32_PAGE_SIGZ              32
#define AT24C32_WRITE_DEVICES          0xA0
#define AT24C32_READ_DEVICES           0xA0
#define AT24C32_LENGTH                 32768     //128*256
#define AT24C32_INIT_LENGTH            4096
#define AT24C32_DELAY_TIME             0x20 


DRV_Status_t AT24C02_Reg_Write(UINT32 SlaveAddr,UINT32 RegAddr, UINT8 *pData, UINT16 Length);
DRV_Status_t AT24C02_Reg_Read(UINT32 SlaveAddr,UINT32 RegAddr, UINT8 *pData, UINT16 Length);
DRV_Status_t AT24C02_Reg_Write_Page(UINT32 SlaveAddr,UINT32 RegAddr, UINT8 *pData, UINT16 Length);
DRV_Status_t AT24C02_Reg_Read_Page(UINT32 SlaveAddr,UINT32 RegAddr, UINT8 *pData, UINT16 Length);
DRV_Status_t AT24C02_Reg_Earse_Page(UINT32 SlaveAddr);





INT8 _IT_AT24C02_I2COpen(void);


#endif

